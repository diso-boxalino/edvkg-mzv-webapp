// This scribt just displays the selected Item on the select Box. 
// For that we have for both boxes a shadow list defined in the file 
// javascribts/org_adressmainscribt.html. That scribt has to be a *.html, so that Boxalino 
// expands the values.
function clickIt(pathprefix, topf)
{
	selChangeFound=false;
	// alert("selectIt was called, topf="+topf+"2");
	for (t=0;t< document.mitResult.elements[topf].length;t++)
	{
		if (document.mitResult.elements[topf].options[t].selected) {
			// alert("/ssi/mit_adressedetailsmall.html?client_session_mitglied="+document.mitResult.elements[topf].options[t].value);
			parent.detailSmall.location.href=pathprefix + "/ssi/mit_adressedetailsmall.html?client_session_mitglied="+document.mitResult.elements[topf].options[t].value;

			break;
		}
		
	}
}

function clickItMit(pathprefix, topf)
{
	selChangeFound=false;
	// alert("selectIt was called, topf="+topf+"2");
	for (t=0;t< document.MitgliederSpeichern.elements[topf].length;t++)
	{
		if (document.MitgliederSpeichern.elements[topf].options[t].selected) {
			// alert("/ssi/mit_adressedetailsmall.html?client_session_mitglied="+document.mitResult.elements[topf].options[t].value);
			parent.detailSmall.location.href=pathprefix + "/ssi/mit_adressedetailsmall.html?client_session_mitglied="+document.MitgliederSpeichern.elements[topf].options[t].value;

			break;
		}
		
	}
}

// just sync the shadow and the lists
function syncShadow(formName, topf_eins, topf_zwei, topfshadow)
{
	alert("SyncShadow");
	for(s=0;s< eval(topfshadow+".length"); s++)
	{
		for (t=0;t< document.MitgliederSpeichern.elements[topf_eins].length;t++)
		{
			if (document.MitgliederSpeichern.elements[topf_eins].options[t].value == eval(topfshadow+"[s][1]"))
			{ // both have the same OID
				eval(topfshadow+"[s][0] = document.MitgliederSpeichern.elements[topf_eins].options[t].selected");
			}				
		}
		for (t=0;t< document.MitgliederSpeichern.elements[topf_zwei].length;t++)
		{
			if (document.MitgliederSpeichern.elements[topf_zwei].options[t].value == eval(topfshadow+"[s][1]"))
			{ // both have the same OID
				eval(topfshadow+"[s][0] = document.MitgliederSpeichern.elements[topf_zwei].options[t].selected");
			}				
		}
		
	}
}

// if somebody uses the push, we habe to resynchronize the values
function superPush(formName,topf,auswahl, shadow)
{
	push(formName, topf, auswahl);
	syncShadow(formName, topf, auswahl, shadow)
}

