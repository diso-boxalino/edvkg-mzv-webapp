function init(myParent) {
	if (myParent.buttonform) {
		if (myParent.buttonform.speichern) myParent.buttonform.speichern.disabled=false;
		if (myParent.buttonform.selectall) myParent.buttonform.selectall.disabled=false;
		if (myParent.buttonform.selectnone) myParent.buttonform.selectnone.disabled=false;
		if (myParent.suche.view_search_cmd) myParent.suche.view_search_cmd.disabled=false;
		if (myParent.wordapplet) myParent.wordapplet.location.href = pathprefix + "/ssi/mit_suchemainwordapplet.html";
	}
}
function resetCount(myParent) {
	if (myParent.frows) {
		myParent.frows.innerHTML = '-';
		if (myParent.frowsMM) myParent.frowsMM.innerHTML = '-';
	}
}
function updateCount(myParent, cnt, cntMM) {
	if (myParent.frows) {
		myParent.frows.innerHTML = cnt;
		if (myParent.frowsMM) {
			if (cntMM === '') cntMM = '-';
			myParent.frowsMM.innerHTML = cntMM;
		}
	}
}
function resetOverride(myParent) {
	if (myParent.suche && myParent.suche.client_request_suchegross) {
		myParent.suche.client_request_suchegross.checked = false;
	}
}
function hideOverride(myParent) {
	if (myParent.mrce) myParent.mrce.style.visibility = "hidden";
}
function showOverride(myParent) {
	if (myParent.mrce) myParent.mrce.style.visibility = "visible";
}
function enableEMailButton (myParent) {
	if (myParent.emailButton) myParent.emailButton.location.href = pathprefix + "/ssi/mit_emailbutton.html";
}
