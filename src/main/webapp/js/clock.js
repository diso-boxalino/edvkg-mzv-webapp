function clientDateTime(clientLanguage, leadingZeros, loginDate, loginTime, pageStartDateTime, clockState, strPageTime, strLoginTime, strSessionTimeout) {

// Session Timeout in Millisekunden definieren
//var sessionTimeout = 1800000;
var sessionTimeout = 5000;
//var sessionTimeout = 7200000;
// Aktuelle Zeit
var Jetzt = new Date();

// Logout?
var pageTime = Jetzt.getTime() - pageStartDateTime;
if (pageTime >= sessionTimeout) {
	pageTime = 0;
//	window.location.href = pathprefix + "/index.html?client_request_exit=true&login_cmd_logout=true";
}

if (clockState == "") {
	var secondsToTen = parseInt(Jetzt.getSeconds() / 10);
	if (secondsToTen == 0 || secondsToTen == 3) {clockState = "dateTime";}
	else if (secondsToTen == 1 || secondsToTen == 4) {clockState = "loginTime";}
	else if (secondsToTen == 2 || secondsToTen == 5) {clockState = "sessionTime";}
}

clockState = "sessionTime";

switch(clockState) {
	case "loginTime":
		// Verstrichene Zeit Login
		//------------------------
		loginDateTime = boxDateTimeToJsDateTime(loginDate, loginTime);
		var loggedInMinutes = parseInt(Jetzt.getTime() / 60000 - loginDateTime.getTime() / 60000);
		var loggedInDays = parseInt(loggedInMinutes / 1440);
		loggedInMinutes = parseInt(loggedInMinutes % 1440);
		var loggedInHours = parseInt(loggedInMinutes / 60);
		loggedInMinutes = parseInt(loggedInMinutes % 60);
		var line1 = strLoginTime + ":";
		var line2 = ((loggedInDays == 0) ? "" : loggedInDays + " D, ") + makeTwoDigits(loggedInHours) + ":" + makeTwoDigits(loggedInMinutes);
		var line3 = "";
		break;

	case "sessionTime":
		// Verstrichene Zeit Page
		//------------------------
		var sessionRemainingTime = sessionTimeout - pageTime;
		var sessionRemainingSeconds = parseInt(sessionRemainingTime / 1000);
		var sessionRemainingDays = parseInt(sessionRemainingSeconds / 86400);
		sessionRemainingSeconds = parseInt(sessionRemainingSeconds % 86400);
		var sessionRemainingHours = parseInt(sessionRemainingSeconds / 3600);
		sessionRemainingSeconds = parseInt(sessionRemainingSeconds % 3600);
		var sessionRemainingMinutes = parseInt(sessionRemainingSeconds / 60);
		sessionRemainingSeconds = parseInt(sessionRemainingSeconds % 60);

		var line1 = strSessionTimeout + ":";
		var line2 = makeTwoDigits(parseInt(sessionRemainingTime / 60000)) + ":" + makeTwoDigits(parseInt((sessionRemainingTime % 60000) / 1000));
		var line2 = ((sessionRemainingDays == 0) ? "" : sessionRemainingDays + " D, ") + makeTwoDigits(sessionRemainingHours) + ":" + makeTwoDigits(sessionRemainingMinutes) + ":" + makeTwoDigits(sessionRemainingSeconds);
		var line3 = "";
		break;

	case "dateTime":
		// Datum, Zeit
		//-------------------
		var Jahr = Jetzt.getYear();
		if(Jahr < 999) Jahr += 1900;
		var Monat = Jetzt.getMonth() + 1;
		var Tag = Jetzt.getDate();
		var WoTag = Jetzt.getDay();
		var Stunden = Jetzt.getHours();
		var Minuten = Jetzt.getMinutes();
		var Sekunden = Jetzt.getSeconds();
		if (leadingZeros) {
			Monat = makeTwoDigits(Monat);
			Tag = makeTwoDigits(Tag);
		}
		
		switch(clientLanguage.substr(0,2).toLowerCase()) {
			case "en":
				var Wochentagname =  new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
				var Datum = Monat  + "/" + Tag  + "/" + Jahr;
				if (Stunden < 12) {
					var amPm = " AM";
				}
				else {
					var amPm = " PM";
				}
				if (Stunden > 12) {
					Stunden = Stunden - 12;
				}
				var Uhrzeit = Stunden + ":" + makeTwoDigits(Minuten) + ":" + makeTwoDigits(Sekunden) + amPm;
				break;
			
			case "fr":
				var Wochentagname =  new Array("Dimanche","Lundi","Mardi","Mercredi","Vendredi","Jeudi","Samedi");
				var Datum = Tag  + "." + Monat  + "." + Jahr;
				var Uhrzeit = makeTwoDigits(Stunden) + ":" + makeTwoDigits(Minuten) + ":" + makeTwoDigits(Sekunden);
				break;

			default:
				var Wochentagname =  new Array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
				var Datum = Tag  + "." + Monat  + "." + Jahr;
				var Uhrzeit = makeTwoDigits(Stunden) + ":" + makeTwoDigits(Minuten) + ":" + makeTwoDigits(Sekunden);
				break;
		}
		
		var WoTagName = Wochentagname[WoTag];
		
		var line1 = WoTagName;
		var line2 = Datum;
		var line3 = Uhrzeit;
		break;
}

// Schreiben
//--------------
if(DHTML) {
//	if(NS) {
//		setCont("id","clientClock1",null,"<span class=\"clock\">" + line1 + "<\/span>");
//		setCont("id","clientClock2",null,"<span class=\"clock\">" + line2  + "<\/span>");
//		setCont("id","clientClock3",null,"<span class=\"clock\">" + line3  + "<\/span>");
//		}
//	else {
		setCont("id","clientClock1",null,line1);
		setCont("id","clientClock2",null,line2);
		setCont("id","clientClock3",null,line3);
//	}
}
else return;

//window.setTimeout("clientDateTime('" + clientLanguage + "', " + leadingZeros + ", '" + loginDate + "', '" + loginTime +"', '" + pageStartDateTime + "', '', '" + strPageTime + "', '" + strLoginTime + "', '" + strSessionTimeout + "')",1000);
}

function makeTwoDigits(oneOrTwoDigits) {
	twoDigits = ((oneOrTwoDigits < 10) ? "0" + oneOrTwoDigits : String(oneOrTwoDigits));
	return twoDigits;
}

function boxDateTimeToJsDateTime(boxDate, boxTime) {
	// boxDate: DD.MM.YYYY or MM/DD/YYYY
	// boxTime: HH:MM oder HH:MM AM/PM
	// Result: Milliseconds since 1.1.1970
	
	// EU-Format
	var splitter1 = ".";
	var splitter2 = "/";
	var splitter3 = ":";
	var splitter4 = " ";
	
	if (boxDate.indexOf(splitter1) > -1) {
		var jsYear = parseInt(boxDate.split(splitter1)[2]);
		var jsMonth = parseInt(boxDate.split(splitter1)[1]) - 1;
		var jsDay = parseInt(boxDate.split(splitter1)[0]);
	}
	if (boxTime.indexOf(splitter4) == -1) {
		var jsHour = parseInt(boxTime.split(splitter3)[0]);
		var jsMinute = parseInt(boxTime.split(splitter3)[1]);
		var jsSecond = parseInt(boxTime.split(splitter3)[2]);
	}
	
	// US-Format
	if (boxDate.indexOf(splitter2) > -1) {
		var jsYear = parseInt(boxDate.split(splitter2)[2]);
		var jsMonth = parseInt(boxDate.split(splitter2)[0]) - 1;
		var jsDay = parseInt(boxDate.split(splitter2)[1]);
	}
	if (boxTime.indexOf(splitter4) > -1) {
		var jsHour = parseInt(boxTime.split(splitter3)[0]);
		if (boxTime.split(splitter4)[1] == "PM") { jsHour = ((jsHour < 12) ? jsHour += 12 : jsHour);}
		var jsMinute = parseInt(boxTime.split(splitter3)[1]);
	}

	var jsDateTime = new Date(jsYear,jsMonth,jsDay,jsHour,jsMinute,0);
	return jsDateTime;
}