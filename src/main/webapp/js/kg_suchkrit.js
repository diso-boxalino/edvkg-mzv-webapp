function setStimmrechtSuche(kg, mainarea, subarea) {
	if (kg >= 1001) return;
	if (document.all["stimmberechtigt"].value != "") {
		document.all["exakteSuche"].disabled = true;
		document.all["exakteSuche"].checked = true;
	}
	else {
		document.all["exakteSuche"].disabled = false;
	}
	
	setExakteSuche(mainarea, subarea);
}