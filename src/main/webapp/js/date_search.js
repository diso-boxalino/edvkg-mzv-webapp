	function trim(inputString) {
		if (typeof inputString != "string") { return inputString; }
		var retValue = inputString;
		var ch = retValue.substring(0, 1);
		while (ch == " ") {
			retValue = retValue.substring(1, retValue.length);
			ch = retValue.substring(0, 1);
		}
		ch = retValue.substring(retValue.length-1, retValue.length);
		while (ch == " ") {
			retValue = retValue.substring(0, retValue.length-1);
			ch = retValue.substring(retValue.length-1, retValue.length);
		}
		return retValue;
	}
	
	function setDateSearchCriteria (name, fieldName, errorText) {
		var ok = true;
		var replaceName = function(name, replace) {
			var el = $("input[name=" + name + "]");
			if (!el.length) el = $("#" + name);
			
//			console.log("setting name ", el, " > " + replace);
			return el.attr("name", replace);
		};
		
		// reset hidden fields
		for (var i = 1; i < 6; i++) {
			replaceName(name + i, "client_request_" + name + i);
		}
		
		var getInputVal = function(name) {
			var val = $("input[name=client_request_" + name + "]").val();
			return val && val.replace(/^\s*/, "").replace(/\s*$/, "") || '';
		};

		// retrieve date elements
		var t1 = getInputVal(name + "tag1");
		var m1 = getInputVal(name + "monat1");
		var j1 = getInputVal(name + "jahr1");
		var t2 = getInputVal(name + "tag2");
		var m2 = getInputVal(name + "monat2");
		var j2 = getInputVal(name + "jahr2");

		// first analysis
		var complete1 = (t1 != '') && (m1 != '') && (j1 != '');
		var complete2 = (t2 != '') && (m2 != '') && (j2 != '');
		var empty1 = (t1 == '') && (m1 == '') && (j1 == '');
		var empty2 = (t2 == '') && (m2 == '') && (j2 == '');
		
//		console.log("complete1 = " + complete1 + ", complete2 = " + complete2 + ", empty1 = " + empty1 + ", empty2 = " + empty2);
		
		// search criteria composition rules
		if (complete1 && complete2) {
			var d1 = new Date(j1, m1, t1);
			var d2 = new Date(j2, m2, t2);
			if (d1 > d2) {
				alert(errorText + "\n" + "(Datum 2 muss gr�sser als Datum 1 sein)");
				ok = false;
			}
			replaceName(name + "1", "view_search_" + fieldName + "_GE").val(t1 + "." + m1 + "." + j1);
			replaceName(name + "2", "view_search_" + fieldName + "_LE").val(t2 + "." + m2 + "." + j2);
		} else if (complete1 && empty2) {
			replaceName(name + "1", "view_search_" + fieldName + "_EQ").val(t1 + "." + m1 + "." + j1);
		} else if (complete2 && empty1) {
			replaceName(name + "2", "view_search_" + fieldName + "_EQ").val(t2 + "." + m2 + "." + j2);
		} else if (!empty1 || !empty2) {
			if (t1 != '') {
				if (t2 == '') replaceName(name + "1", "view_search_" + fieldName + "_DAY_EQ").val(t1); 
				if (t2 != '') replaceName(name + "1", "view_search_" + fieldName + "_DAY_GE").val(t1);
			}
			if (t2 != '') {
				if (t1 == '') replaceName(name + "2", "view_search_" + fieldName + "_DAY_EQ").val(t2);
				if (t1 != '') replaceName(name + "2", "view_search_" + fieldName + "_DAY_LE").val(t2);
			}
			if (m1 != '') {
				if (m2 == '') replaceName(name + "3", "view_search_" + fieldName + "_MONTH_EQ").val(m1);
				if (m2 != '') replaceName(name + "3", "view_search_" + fieldName + "_MONTH_GE").val(m1);
			}
			if (m2 != '') {
				if (m1 == '') replaceName(name + "4", "view_search_" + fieldName + "_MONTH_EQ").val(m2);
				if (m1 != '') replaceName(name + "4", "view_search_" + fieldName + "_MONTH_LE").val(m2);
			}
			if (j1 != '') {
				if (j2 == '') replaceName(name + "5", "view_search_" + fieldName + "_YEAR_EQ").val(j1);
				if (j2 != '') replaceName(name + "5", "view_search_" + fieldName + "_YEAR_GE").val(j1);
			}
			if (j2 != '') {
				if (j1 == '') replaceName(name + "6", "view_search_" + fieldName + "_YEAR_EQ").val(j2);
				if (j1 != '') replaceName(name + "6", "view_search_" + fieldName + "_YEAR_LE").val(j2);
			}
		} else if (empty1 && empty2) {
			// ok
		} else {
			alert(errorText);
			ok = false;
		}
		return ok;
	}

  // Inserted by mh@20060620:
  // set the search fields
  function setDatePart(formField, datePart, valueToSplit, exactValue) {
    if (formField == null || (valueToSplit == "" && exactValue == "") || formField == "") return;
    var part = "";
    if (exactValue != "") {
      part = exactValue;
    } else {
      var ind1 = valueToSplit.indexOf(".");
      var ind2 = valueToSplit.lastIndexOf(".");
      if (ind1 == 0 || ind2 == 0) return;
      if (datePart == 1) {
        part = valueToSplit.slice(0, ind1);
      } else if (datePart == 2) {
        part = valueToSplit.slice(ind1 + 1, ind2);
      } else {
        part = valueToSplit.slice(ind2 + 1);
      }
    }
    var inputField = document.all[formField];
    if (inputField != null && part != null && part != "") inputField.value = part;
  }
