/* DHTML-Bibliothek */

var DHTML = 0, DOM = 0, MS = 0, NS = 0, OP = 0;

function DHTML_init() {

 if (window.opera) {
     OP = 1;
 }
 if(document.getElementById) {
   DHTML = 1;
   DOM = 1;
 }
 if(document.all && !OP) {
   DHTML = 1;
   MS = 1;
 }
if (window.netscape && window.screen && !DOM && !OP){
   DHTML = 1;
   NS = 1;
 }
}

// Diese Funktion ist eigentlich die zentrale Funktion der hier vorgestellten Biblothek. Sie greift auf ein bestimmtes Element im Dokument zu und gibt das Objekt dieses Elements zur�ck. Das funktioniert mit DOM-f�higen Browsern ebenso wie mit dem �lteren Internet Explorer 4.x und in eingeschr�nkter Form (nur im Zusammenhang mit layer- oder div-Elementen) auch mit Netscape 4.x.
// In den meisten F�llen m�ssen Sie diese Funktion nicht selber aufrufen. Es handelt sich vielmehr um eine Basisfunktion, die von den �brigen Funktionen der Bibliothek benutzt wird. Dennoch gibt es auch F�lle, in denen ein Direktaufruf dieser Funktion Sinn macht. Die Funktion erwartet folgende Parameter:
//p1 ist die Art und Weise, wie auf ein Element zugegriffen werden soll. Es gibt vier erlaubte Werte f�r diesen Parameter: "id", "name", "tagname" und "index". �bergeben Sie "id", wenn Sie auf ein Element zugreifen wollen, das ein id-Attribut hat, z.B. <div id="Bereich">. �bergeben Sie "name", wenn das Element, auf das Sie zugreifen wollen, kein id-Attribut, aber daf�r ein name-Attribut hat, z.B. <a name="Anker">. �bergeben Sie "tagname", wenn das Element, auf das Sie zugreifen wollen, weder ein id- noch ein name-Attribut hat. �bergeben Sie "index" nur dann, wenn es sich um Netscape-Layer handelt, die �ber das layers-Objekt mit Hilfe von Indexnummern angesprochen werden sollen.
//p2 ist die n�here Angabe zu dem, was Sie bei p1 festgelegt haben: wenn Sie bei p1 den Wert "id" �bergeben, dann erwartet die Funktion bei p2 den Wert des id-Attributs. Wenn Sie bei p1 den Wert "name" �bergeben, dann erwartet p2 den Wert des name-Attributs. Wenn Sie bei p1 den Wert tagname �bergeben haben, dann erwartet p2 den gew�nschten Elementnamen, also z.B. h1 oder div. Wenn Sie bei p1 den Wert "index" �bergeben haben, erwartet p2 die Indexnummer f�r den gew�nschten Netscape-Layer.
//p3 wird nur dann ben�tigt, wenn bei p1 einer der beiden Werte "name" oder "tagname" angegeben wurde. In diesem Fall m�ssen Sie bei p3 eine Indexnummer angeben, mit der Sie festlegen, das wie vielte Element im Dokument mit dem entsprechenden Namen oder Elementnamen Sie meinen. Das erste Element hat die Indexnummer 0, das zweite Nummer 1 usw.
//�bergeben Sie bei Parametern, f�r die Sie keinen Wert angeben, einfach das Wort null (ohne Anf�hrungszeichen!).
function getElem(p1,p2,p3) {
 var Elem;
 if(DOM) {
   if(p1.toLowerCase()=="id") {
     if (typeof document.getElementById(p2) == "object")
     Elem = document.getElementById(p2);
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="name") {
     if (typeof document.getElementsByName(p2) == "object")
     Elem = document.getElementsByName(p2)[p3];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="tagname") {
     if (typeof document.getElementsByTagName(p2) == "object" || (OP && typeof document.getElementsByTagName(p2) == "function"))
     Elem = document.getElementsByTagName(p2)[p3];
     else Elem = void(0);
     return(Elem);
   }
   else return void(0);
 }
 else if(MS) {
   if(p1.toLowerCase()=="id") {
     if (typeof document.all[p2] == "object")
     Elem = document.all[p2];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="tagname") {
     if (typeof document.all.tags(p2) == "object")
     Elem = document.all.tags(p2)[p3];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="name") {
     if (typeof document[p2] == "object")
     Elem = document[p2];
     else Elem = void(0);
     return(Elem);
   }
   else return void(0);
 }
 else if(NS) {
   if(p1.toLowerCase()=="id" || p1.toLowerCase()=="name") {
   if (typeof document[p2] == "object")
     Elem = document[p2];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="index") {
    if (typeof document.layers[p2] == "object")
     Elem = document.layers[p2];
    else Elem = void(0);
     return(Elem);
   }
   else return void(0);
 }
}

// �ber diese Funktion, die ihrerseits auf die Funktion getElem() zur�ckgreift, k�nnen Sie auf bequeme Weise den Inhalt eines Elements ermitteln - zumindest wenn das DOM-Modell oder das �ltere Microsoft-Modell f�r DHTML verf�gbar sind. Das �ltere Netscape-Modell kennt kein Auslesen von Elementinhalten. In den Beispielaufrufen ist der Funktionsaufruf deshalb davon abh�ngig gemacht, dass nicht nach �lterer Netscape-Syntax gearbeitet wird (if(!NS)).
// Die Funktion liefert bei Verwendung des DOM-Modells eine leere Zeichenkette zur�ck, falls der Inhalt des Element kein Text ist, sondern ein weiteres Element. Wenn also beispielsweise notiert ist:
// <p>der Text</p>
// Dann liefert die Funktion den Wert der Text zur�ck. Wenn aber notiert ist:
// <p><b>der Text</b></p>
// Dann liefert die Funktion bei Anwendung des DOM-Modells eine leere Zeichenkette zur�ck, da es keinen Text als Elementinhalt von p gibt, sondern der Inhalt ein anderes Element, n�mlich ein b-Element ist.
// Die Parameter sind bei getCont() die gleichen wie bei getElem(). Nur der Wert "index" bei Parameter p1 wird von dieser Funktion nicht verarbeitet. �bergeben Sie bei Parametern, f�r die Sie keinen Wert angeben, das Wort null.
function getCont(p1,p2,p3) {
   var Cont;
   if(DOM && getElem(p1,p2,p3) && getElem(p1,p2,p3).firstChild) {
     if(getElem(p1,p2,p3).firstChild.nodeType == 3)
       Cont = getElem(p1,p2,p3).firstChild.nodeValue;
     else
       Cont = "";
     return(Cont);
   }
   else if(MS && getElem(p1,p2,p3)) {
     Cont = getElem(p1,p2,p3).innerText;
     return(Cont);
   }
   else return void(0);
}

// �ber diese Funktion, die ihrerseits auf die Funktion getElem() zur�ckgreift, k�nnen Sie auf bequeme Weise den Wert eines Attributs in einem Element ermitteln - zumindest wenn das DOM-Modell oder das �ltere Microsoft-Modell f�r DHTML verf�gbar sind. F�r einige Objekte ist es auch im Netscape 4 m�glich. Neben den drei bereits bekannten ersten Parametern erwartet diese Funktion einen vierten Parameter p4. Bei diesem m�ssen Sie den Namen des gew�nschten Attributs �bergeben. �bergeben Sie bei Parametern, f�r die Sie keinen Wert angeben, das Wort null.
function getAttr(p1,p2,p3,p4) {
   var Attr;
   if((DOM || MS) && getElem(p1,p2,p3)) {
     Attr = getElem(p1,p2,p3).getAttribute(p4);
     return(Attr);
   }
   else if (NS && getElem(p1,p2)) {
       if (typeof getElem(p1,p2)[p3] == "object")
        Attr=getElem(p1,p2)[p3][p4]
       else
        Attr=getElem(p1,p2)[p4]
         return Attr;
       }
   else return void(0);
}

// Mit dieser Funktion k�nnen Sie den Inhalt eines Elements dynamisch �ndern. Die Funktion setCont() bedient sich f�r den Zugriff auf das gew�nschte Element der Basisfunktion getElem(). Die ersten drei Parameter, die bei dieser Funktion die gleichen sind wie bei den �brigen Funktionen, werden dazu an getElem() weitergereicht. Als vierten Parameter p4 erwartet die Funktion setCont() den gew�nschten Textinhalt. Beachten Sie, dass dieser Text zumindest beim DOM-Modell und beim �lteren Microsoft-Modell nur als reiner Text interpretiert wird. HTML-Formatierungen im Text werden dabei als Klartext interpretiert.
// �bergeben Sie bei Parametern, f�r die Sie keinen Wert angeben, das Wort null.
function setCont(p1,p2,p3,p4) {
   if(DOM && getElem(p1,p2,p3) && getElem(p1,p2,p3).firstChild)
     getElem(p1,p2,p3).firstChild.nodeValue = p4;
   else if(MS && getElem(p1,p2,p3))
     getElem(p1,p2,p3).innerText = p4;
   else if(NS && getElem(p1,p2,p3)) {
     getElem(p1,p2,p3).document.open();
     getElem(p1,p2,p3).document.write(p4);
     getElem(p1,p2,p3).document.close();
   }
}

DHTML_init();